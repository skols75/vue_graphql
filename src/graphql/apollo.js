import Vue from "vue";
import VueApollo from "vue-apollo";

import ApolloClient from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

// Register the VueApollo plugin with Vue
Vue.use(VueApollo);

// HTTP connection to the API
const httpLink = new HttpLink({
  uri: "http://localhost:5000/graphql",
});

// Creating another variable here because it makes it easier to add more links in the future
const link = httpLink;

// Cache implementation
const cache = new InMemoryCache();

// Create the apollo client
const apolloClient = new ApolloClient({
  link,
  cache,
  connectToDevTools: true,
});

const apolloProvider = new VueApollo({
  // Apollo 2.0 allows multiple clients to be enabled at once
  // Here we select the default (and only) client
  defaultClient: apolloClient,
});

export default apolloProvider;
